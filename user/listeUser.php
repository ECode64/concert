<?php
include 'connexionDB.php';
$sql = "SELECT * FROM user";
if ($conn->query($sql)) {
    $result= $conn->query($sql);
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

if(isset($_POST['mod'])) {
    $uid = $_POST['uid'];
    $u = $_POST['userName'];
    $n = $_POST['nom'];
    $pr = $_POST['prenom'];
    $e = $_POST['mail'];
}
if (isset($_POST['sup'])){
    $uidSup = $_POST['uid'];

    $sql2 ="DELETE FROM user WHERE userId = $uidSup";
    $conn->query($sql2);
    echo "<script> location.replace('?menu=listeUser'); </script>";
}
?>

<title>Liste des clients</title>

<div class="col-lg-10 col-lg-offset-1">
    <legend><h2>Liste des clients</h2></legend>
    <div class="table-responsive">
        <table class="table table-dark" id="tab">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>E-mail</th>
                <th>Username</th>
                <th></th>
            </tr>
            </thead>
            <?php
            $var = '<tbody>';
            // REMPLACEMENT DU BUTTON TYPE 'BUTTON' PAR 'SUBMIT' LIGNE 62 POUR POSTER LES DONNEES (OBLIGATOIRE)
            // LA REDIRECTION SE FAIT LIGNE 48 AVEC UNE VARIABLE '&display=true' POUR AFFICHER LA MODALE...
            while($row = mysqli_fetch_array($result)){
                echo "<form method=\"post\" action=\"\">";
                echo "<tr>";

                echo "<td>" . $row['userId'] ."<input type='hidden' name='uid' value= '".$row['userId']."'>". "</td>";
                echo "<td>" . $row['nom']  ."<input type='hidden' name='nom' value= '".$row['nom']."'>"."</td>";
                echo "<td>" . $row['prenom'] ."<input type='hidden' name='prenom' value= '".$row['prenom']."'>"."</td>";
                echo "<td>" . $row['email'] ."<input type='hidden' name='mail' value= '".$row['email']."'>"."</td>";
                echo "<td>" . $row['username']."<input type='hidden' name='userName' value= '".$row['username']."'>"."</td>";

                echo " <td><button type=\"submit\" class=\"buttonTable\" name=\"mod\" style=\"width:auto;\">Modifier</button>
                            <button type=\"submit\" class=\"buttonTable\" name=\"sup\" style=\"width:auto;\" >Suprimer</button></td>
            </tr>";
                echo "</form>";
            }?>
            </tbody>
        </table>
    </div>
</div>

<div>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resources/css/inscription.css">
    </head>
    <div id="id-3" class="modal">

        <form class="modal-content animate" action="#" method="post">
            <div class="imgcontainer img-responsive">
                <span onclick="document.getElementById('id-3').style.display='none'" class="close" title="Close Modal">&times;</span>
                <h2>Modifier user</h2>
                <img src="/resources/images/avatar.png" alt="Avatar" class="avatar">
            </div>

            <div class="container-fluid col-lg-offset-1">
                <br><label for="id">ID user:</label><br>
                <input type="text" class="form-control" name="uidM"   title="Si vous modifiez l'ID aucune modification sera effectue!"
                       value=<?php echo $uid; ?>><br>
                <label>Si vous modifiez l'ID aucune modification sera effectue!</label><br>
                <br><label for="nom">Nom :</label><br>
                <input type="text" class="form-control" name="nomMod" required placeholder="Entrez votre nom."
                       value= <?php echo $n; ?> pattern="[A-Za-z]{3,20}"
                       title="Entrez minimum trois lettres de votre nom (aucun caractere speciaux ni chiffre est valide)."><br>

                <br><label for="prenom">Prenom :</label><br>
                <input type="text" class="form-control" name="prenomMod" required placeholder="Entrez votre prenom."
                       value= <?php echo $pr; ?> pattern="[A-Za-z]{3,20}"
                       title="Entrez minimum trois lettres de votre prenom (aucun caractere speciaux ni chiffre est valide)."><br>
                <br>

                <br><label for="mail">E-Mail:</label><br>
                <input type="text" class="form-control" name="mailMod" required placeholder="Entrez votre E-Mail."
                       value= <?php echo $e; ?>><br>

                <br><label for="userName">Username:</label><br>
                <input type="text" class="form-control" name="userNameMod" required
                       placeholder="Entrez votre username"
                       value= <?php echo $u; ?> pattern="[A-Za-z0-9]{5,20}"
                       title="L'username doit contenir cinq lettre ou chiffres"><br>
                <button type="submit" name="modifier" class="buttonInscForm">Modifier</button>
                <br>

            </div>
            <div class="container-fluid col-lg-offset-1 col-lg-pull-2" style="background-color:#0f0f0f  width: 50% ">
                <button type="button" onclick="document.getElementById('id-3').style.display='none'" class="cancelbtnFormInsc">
                    Cancel
                </button>
            </div>
            <br>
        </form>
    </div>

    <?php
    if (isset($_POST['nomMod'])) { $nMod = $_POST['nomMod']; }
    if (isset($_POST['prenomMod'])) { $prMod = $_POST['prenomMod']; }
    if (isset($_POST['mailMod'])) { $eMod = $_POST['mailMod']; }
    if (isset($_POST['userNameMod'])) { $uMod = $_POST['userNameMod']; }
    if(isset($_POST['modifier'])) {
        $uidMo ="";
        $uidMo = $_POST['uidM'];
        $sql4 = "UPDATE  user 
    SET nom = '$nMod', prenom = '$prMod', email = '$eMod', username = '$uMod'
    WHERE userId = $uidMo";
        $conn->query($sql4);
        echo "<script> location.replace('?menu=listeUser'); </script>";
    }
    if(isset($_POST['mod']))
    {
        ?>
        <script>
            document.getElementById('id-3').style.display='block';
        </script>
        <?php
    }
    ?>

    <?php
        $conn->close();
    ?>
    <script>
        // Get the modal
        var modal = document.getElementById('id-3');

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {

            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
</div>
