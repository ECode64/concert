<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resources/css/login.css">
</head>
<button class="buttonLog btn" onclick="document.getElementById('id-2').style.display='block'" style="width:auto;">Login</button>

<div id="id-2" class="modal">

    <form class="modal-content animate" action="#" method="post">
        <div class="imgcontainer container-fluid">
            <span onclick="document.getElementById('id-2').style.display='none'" class="close" title="Close Modal">&times;</span>
            <h2>Login</h2>
            <img src="/resources/images/avatar.png" alt="Avatar" class="avatar">
        </div>

        <div class="container-fluid col-lg-offset-1">
            <label for="userName"><span class="glyphicon glyphicon-user"></span><b> Username ou Adresse e-mail.</b></label><br>
            <input type="text" placeholder="Enter Username." name="userName" required autofocus >
            <p id="userNameError" style="color: white" ></p>
        </div>
        <div class="container-fluid col-lg-offset-1">
            <label for="pwd"><span class="glyphicon glyphicon-eye-open"></span><b> Password.</b></label><br>
            <input type="password" placeholder="Enter Password." name="pwd" required autofocus>
            <p id="pwdInv" style="color: white" ></p>
        </div>
        <div class="container-fluid col-lg-offset-1">
            <button type="submit" name="login" class="buttonLogForm">Login</button>
        </div>

        <div class="container-fluid col-lg-offset-1" style="background-color:#0f0f0f  width: 50% ">
            <button type="button" onclick="document.getElementById('id-2').style.display='none'" class="cancelbtnFormLog">Cancel</button>
        </div><br>
    </form>
</div>
<script>
    var modal = document.getElementById('id-2');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<?php

include 'connexionDB.php';
$u=$p=$username=$pwd=$userId=$role=$mail="";
if (isset($_POST['userName'])) { $u = $_POST['userName']; }
if (isset($_POST['pwd'])) { $p = $_POST['pwd']; }
if(isset($_POST['login'])){
    $sql = "SELECT * FROM user WHERE username = '$u' OR email = '$u'";
    $res = $conn->query($sql);
    while($row = mysqli_fetch_array($res)) {
        $userId = $row['userId'];
        $username = $row['username'];
        $mail = $row['email'];
        $pwd = $row['password'];
    }
    $sql2 = "SELECT * FROM user_role WHERE userId = '$userId'";
    $res2 = $conn->query($sql2);
    while ($row2 = mysqli_fetch_array($res2)){
        $role = $row2['role'];
    }
    if (($username == $u || $mail == $u) && password_verify($p, $pwd)) {

        $_SESSION['userIdAct'] = $userId;
        $_SESSION['userNameAct'] = $username;
        $_SESSION['userRoleAct'] = $role;
        if($role == 'ROLE_ADMIN'){
            echo "<script> location.replace('?menu=index'); </script>";
        }else{
            echo "<script> location.replace('?menu=concerts'); </script>";
        }
    }else{
        if($username == null || $mail == null){
            echo "<script>
                document.getElementById('id-2').style.display='block';
                document.getElementById('userNameError').innerHTML = '<div class=\"alert alert-danger\"> <strong>Error!</strong> $u n\'existe pas.</div>' ;
            </script>";
        }
        if ($username == $u || $mail == $u && password_verify($p, $pwd)){
            echo "<script>
            document.getElementById('id-2').style.display='block';
            document.getElementById('pwdInv').innerHTML = '<div class=\"alert alert-danger\"> <strong>Error!</strong> mot passe incorrect.</div>' ;
            </script>";
            }
    }
}
$conn->close();
?>
