
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resources/css/inscription.css">
</head>
<button class="buttonInsc btn" onclick="document.getElementById('id-1').style.display='block'" style="width:auto;">S'inscrire</button>

<div id="id-1" class="modal">

    <form class="modal-content animate" action="#" method="post">
        <div class="imgcontainer img-responsive">
            <span onclick="document.getElementById('id-1').style.display='none'" class="close" title="Close Modal">&times;</span>
            <h2>Inscription</h2>
            <img src="/resources/images/avatar.png" alt="Avatar" class="avatar">
        </div>

        <div class="container-fluid col-lg-offset-1">
            <label for="nom">Nom :</label><br>
            <input type="text" class="form-control" id="nom" name="nom" required placeholder="Entrez votre nom."
                   pattern="[A-Za-z]{3,20}" autofocus
                   title="Entrez minimum trois lettres de votre nom (aucun caractere speciaux ni chiffre est valide)." >
        </div><br>
        <div class="container-fluid col-lg-offset-1">
            <label for="prenom">Prenom :</label><br>
            <input type="text" class="form-control" id="prenom" name="prenom" required placeholder="Entrez votre prenom."
                   pattern="[A-Za-z]{3,20}"
                   title="Entrez minimum trois lettres de votre prenom (aucun caractere speciaux ni chiffre est valide)." >
        </div><br>
        <div class="container-fluid col-lg-offset-1">
            <label for="mail">E-Mail:</label><br>
            <input type="text" class="form-control" id="mail" name="mail" required placeholder="Entrez votre E-Mail.">
            <p id="email" style="color: white" ></p>
        </div><br>
        <div class="container-fluid col-lg-offset-1">
            <label for="userName">Username:</label><br>
            <input type="text" class="form-control" id="userName" name="userName" required placeholder="Entrez votre username."
                   pattern="[A-Za-z0-9]{5,20}"
                   title="L'username doit contenir cinq lettre ou chiffres">
            <p id="user" style="color: white" ></p>
        </div><br>
        <div class="container-fluid col-lg-offset-1">
            <label for="pwd">Password:</label><br>
            <input type="password" class="form-control" id="pwd" name="pwd" required placeholder="Entrez votre password.">
        </div>
        <p id="success" class="col-lg-offset-1"></p>
        <div class="container-fluid col-lg-offset-1">
            <button type="submit" class="btn buttonInscForm" name="inscription">S'inscrire</button>
        </div>
        <div class="container-fluid col-lg-offset-1">
            <button type="button" onclick="document.getElementById('id-1').style.display='none'" class="cancelbtnFormInsc">Cancel</button>
        </div><br>
    </form>
</div>

<script>
    // Get the modal
    var modal = document.getElementById('id-1');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<?php
include 'connexionDB.php';
$n =$p = $e=$u=$p =$sql=$sql2=$sql3=$sql4=$sql5=$row="";

if (isset($_POST['nom'])) { $n = $_POST['nom']; }
if (isset($_POST['prenom'])) { $pr = $_POST['prenom']; }
if (isset($_POST['mail'])) { $e = $_POST['mail']; }
if (isset($_POST['userName'])) { $u = $_POST['userName']; }
if (isset($_POST['pwd'])) { $p = $_POST['pwd']; }
$pwdCrypt = password_hash($p, PASSWORD_BCRYPT);
if(isset($_POST['inscription'])){

    $sql = "SELECT username, email FROM user WHERE username = '$u' or email = '$e'";
    $res = $conn->query($sql);
    while($row = mysqli_fetch_array($res)) {
            if ($row['username'] == $u || $row['email'] == $e) {
                if ($row['username'] == $u) {
                    echo "<script>
                            document.getElementById('id-1').style.display='block';
                            document.getElementById('user').innerHTML = '<div class=\"alert alert-danger\"> <strong>Error!</strong> $u existe déjà!.</div>' ;
                        </script>";
                }
                if ($row['email'] == $e) {
                    echo "<script>
                            document.getElementById('id-1').style.display='block' ;
                            document.getElementById('email').innerHTML = '<div class=\"alert alert-danger\"> <strong>Error!</strong> $e existe déjà!.</div>' ;
                          </script>";
                }
            }
        }
        $sql2 = "INSERT INTO user (nom, prenom, email, username, password)
                VALUES ('$n','$pr','$e','$u', '$pwdCrypt')";
        if ($conn->query($sql2)) {
            $sql3 = "SELECT userId FROM user WHERE username = '$u'";
            $res2 = $conn->query($sql3);
            while($row2 = mysqli_fetch_array($res2)) {
                $userId = $row2['userId'];
                if ($userId == 1){
                    $sql4 ="INSERT INTO user_role(role,userId)
                                  VALUE ('ROLE_ADMIN','$userId')";
                    $conn->query($sql4);
                }else{
                    $sql5 ="INSERT INTO user_role(role, userId)
                                  VALUE ('ROLE_USER','$userId')";
                    $conn->query($sql5);
                }
            }
            $conn->close();
        }
}

?>
