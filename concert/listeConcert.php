<?php
include 'connexionDB.php';
$sql = "SELECT * FROM concert";
if ($conn->query($sql)) {
    $res=$conn->query($sql);
}else{
    echo "Error: " . $sql . "<br>" . $conn->error;
}

?>
<?php
if(isset($_POST['mod'])) {
    $cid = $_POST['cid'];
    $nC = $_POST['nC'];
    $nbArt = $_POST['nbArt'];
    $nbPlaces = $_POST['nbPlaces'];
    $dtC = $_POST['dtC'];
    $pxP = $_POST['pxP'];
}
if (isset($_POST['sup'])){
    $cidSup = $_POST['cid'];

    $sql2 ="DELETE FROM concert WHERE concertId = $cidSup";
    $conn->query($sql2);
    echo "<script> location.replace('?menu=listeConcert'); </script>";
}
?>
<title>Liste des concert</title>
<div class="col-lg-10 col-lg-offset-1">
    <legend><h2>Liste des concert</h2></legend>
    <div class="table-responsive ">
        <table class="table table-dark">
            <thead>
            <tr>
                <th>ID cooncert</th>
                <th>Nom concert</th>
                <th>Nombre des places</th>
                <th>Nombre d'artistes</th>
                <th>Date concert</th>
                <th>Prix place</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <?php
                while($row = mysqli_fetch_array($res)) {
                    echo "<tr>";
                    echo "<form method=\"post\" action=\"\">";
                    echo "<td>" . $row['concertId'] . "<input type='hidden' name='cid' value= '" . $row['concertId'] . "'>" . "</td>";
                    echo "<td>" . $row['nomConcert'] . "<input type='hidden' name='nC' value= '" . $row['nomConcert'] . "'>" . "</td>";
                    echo "<td>" . $row['nbrePlaces'] . "<input type='hidden' name='nbPlaces' value= '" . $row['nbrePlaces'] . "'>" . "</td>";
                    echo "<td>" . $row['nbreArtistes'] . "<input type='hidden' name='nbArt' value= '" . $row['nbreArtistes'] . "'>" . "</td>";
                    echo "<td>" . $row['dateconcert'] . "<input type='hidden' name='dtC' value= '" . $row['dateconcert'] . "'>" . "</td>";
                    echo "<td>" . $row['prixPlace'] . " € <input type='hidden' name='pxP' value= '" . $row['prixPlace'] . "'>" . "</td>";

                    echo " <td> <button type=\"submit\" class=\"buttonTable\" name='mod' style=\"width:auto;\">Modifier</button>
                                <button type=\"submit\" class=\"buttonTable\" name='sup' style=\"width:auto;\" >Suprimer</button></td>";
                    echo " </tr> </form>";
                }?>
            </tbody>
        </table>
    </div>
</div>
<div>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resources/css/inscription.css">
    </head>
    <div id="id-4" class="modal">

        <form class="modal-content animate" action="#" method="post">
            <div class="imgcontainer img-responsive">
                <span onclick="document.getElementById('id-4').style.display='none'" class="close" title="Close Modal">&times;</span>
                <h2>Modifier concert</h2>
            </div>

            <div class="container-fluid col-lg-offset-1">
                <br><label for="cidMod">ID concert:</label><br>
                <input type="text" class="form-control" name="cidMod"   title="Si vous modifiez l'ID aucune modification sera effectue!"
                       value=<?php echo $cid; ?>><br>
                <label>Si vous modifiez l'ID aucune modification sera effectue!</label><br>

                <br><label for="nCMod">Nom concert :</label><br>
                <input type="text" class="form-control" name="nCMod" required placeholder="Entrez votre nom"
                       value= <?php echo $nC; ?> pattern="[A-Za-z]{3,20}"
                       title="Entrez minimum trois lettres du nom du concert (aucun caractere speciaux ni chiffre est valide)"><br>

                <br><label for="nbPlacesMod">Nombre places :</label><br>
                <input type="text" class="form-control" name="nbPlacesMod" required placeholder="Nombre de places. Ex:2000"
                       value= <?php echo $nbPlaces; ?> pattern="[0-9]{3,20}" title="Minimun trois chiffres"><br>

                <br><label for="nbArtMod">Nombre Artistes:</label><br>
                <input type="text" class="form-control" name="nbArtMod" required placeholder="Nombre d'Artistes. Ex: 10"
                       value= <?php echo $nbArt; ?>><br>

                <br><label for="dtCMod">Date concert:</label><br>
                <input type="date" max="2020-12-31" min="<?php echo date("Y-m-d",strtotime("+1 week"))?>"
                       class="form-control" id="dateConcert" name="dtCMod" required="true" value= <?php echo $dtC; ?>
                       placeholder="Entrez la date du concert"><br>

                <br><label for="pxPMod">Prix place:</label><br>
                <input type="text" class="form-control" name="pxPMod" required
                       placeholder="Entrez le prix. Ex: 90.50"
                       value= <?php echo $pxP; ?>
                       title="Entrez le prix. Ex: 90.50"><br>

                <button type="submit" name="modifier" class="buttonInscForm">Modifier</button>
                <br>

            </div>
            <div class="container-fluid col-lg-offset-1 col-lg-pull-2" style="background-color:#0f0f0f  width: 50% ">
                <button type="button" onclick="document.getElementById('id-4').style.display='none'" class="cancelbtnFormInsc">
                    Cancel
                </button>
                <!--  <button type="submit" name="sup">Suprimer</button> -->
            </div>
            <br>
        </form>
    </div>

    <?php
    // SI APRES LE CHARGEMENT DE LA PAGE LE DISPLAY EST 'true', ALORS ON AFFICHE LA MODALE
    if (isset($_POST['nCMod'])) { $nCMod = $_POST['nCMod']; }
    if (isset($_POST['nbPlacesMod'])) { $nbPlacesMod = $_POST['nbPlacesMod']; }
    if (isset($_POST['nbArtMod'])) { $nbArtMod = $_POST['nbArtMod']; }
    if (isset($_POST['dtCMod'])) { $dtCMod = $_POST['dtCMod']; }
    if (isset($_POST['pxPMod'])) { $pxPMod = $_POST['pxPMod']; }

    if(isset($_POST['modifier'])) {
        $cidMod ="";
        $cidMod = $_POST['cidMod'];
        $sql4 = "UPDATE  concert
    SET nomConcert = '$nCMod', nbrePlaces = '$nbPlacesMod', nbreArtistes = '$nbArtMod', dateconcert = '$dtCMod',prixPlace ='$pxPMod'
    WHERE concertId = $cidMod";
        $conn->query($sql4);
       echo "<script> location.replace('?menu=listeConcert'); </script>";
    }
    if(isset($_POST['mod']))
    {
        ?>
        <script>
            document.getElementById('id-4').style.display='block';
        </script>
        <?php
    }
    // FIN MODIFICATIONS
    ?>

    <?php
    $conn->close();
    ?>
    <script>
        // Get the modal
        var modal = document.getElementById('id-4');

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {

            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
</div>
