<?php
	include 'connexionDB.php';
	if(isset($_POST['submit']))
	{
		// Recuperer valeurs formulaire
		$ticket 	= $_POST['selectTag'];
		$nom 		= $_POST['nom'];
		$prixPlace	= $_POST['prixPlace'];
		$nbPlaces 	= $_POST['nbPlaces'];
		$date 		= $_POST['date'];


		global $panier;
		$_SESSION['nbTickets'] 	= $ticket;
		$_SESSION['nom'] 		= $nom;
		$_SESSION['prixPlace'] 	= $prixPlace;
		$_SESSION['nbPlaces'] 	= $nbPlaces;
		$_SESSION['date'] 		= $date;

		/*$string = "UPDATE concert SET nbrePlaces = nbrePlaces - '$ticket' WHERE nomConcert = '$nom';";
		$conn->query($string);*/
		//if(session_id() != '' || isset($_SESSION))
		if(isset($_SESSION['userNameAct']))
		{
			//echo '<script>window.location.href="?menu=validation"</script>';
			?>
				<script>window.location.href="?menu=validation"</script>
			<?php
		}
		else
		{
            echo "<script>
                document.getElementById('id-2').style.display='block';
            </script>";

		}
	}
?>

<title>Achat ticket</title>

<div class="container col-lg-10 col-lg-offset-1" style="color:white;">
    <div><legend><h2>Concerts</h2></legend></div>
<?php  
	include 'connexionDB.php';
	$response = $conn->query('SELECT * FROM concert');

	if($response)
	{
		createTable($response);
	}

	function createTable($arrayToDisplay){
		echo "<table class='table table-dark'>";
		echo "<tr><th scope='col'>CONCERT</th><th scope='cool'>PRIX</th><th scope='col'>PLACES DISPONIBLES</th>";
		echo "<th scope='cool'>DATE</th><th scope='col'>NOMBRE DE PLACES</th><th scope='col'></th></tr>";
		foreach ($arrayToDisplay as $key => $value) {
			echo "<form method='post'>";
			echo "<tr style='color:white;'>";
			echo "<td>";
			echo $value['nomConcert'];
			echo "<input type='hidden' name='nom' value='".$value['nomConcert']."'></input>";
			echo "</td>";
			echo "<td>";
			echo $value['prixPlace']." Euros";
			echo "<input type='hidden' name='prixPlace' value='".$value['prixPlace']."'></input>";
			echo "</td>";
			echo "<td>";
			echo $value['nbrePlaces'];
			echo "<input type='hidden' name='nbPlaces' value='".$value['nbrePlaces']."'></input>";
			echo "</td>";
			echo "<td>";
			echo $value['dateconcert'];
			echo "<input type='hidden' name='date' value='".$value['dateconcert']."'></input>";
			echo "</td>";
			echo "<td>";
			echo "<select class='form-control' name='selectTag' value='selected'>";
			if($value['nbrePlaces']>20)
			{
				for($i = 1; $i <= 20; $i++)
				{
					echo "<option style='visibility: visible;' value='".$i."'>".$i."</option>";
				}
			}
			else
			{
				$nbPlaceRestantes = $value['nbrePlaces'];
				while($nbPlaceRestantes>0)
				{
					echo "<option style='visibility: visible;' value='".$nbPlaceRestantes."'>".$nbPlaceRestantes."</option>"; 
					$nbPlaceRestantes--;
				}
			}
			echo "</select></td><td><input class='btn btn-primary' name='submit' type='submit' value='valider'></td>";
			echo "</td></tr></form>";
		}
		echo "</table>";
	}
?>

</div>