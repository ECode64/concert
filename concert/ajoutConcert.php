<title>Ajouter concert</title>
<head>
    <link rel="stylesheet" href="resources/css/style.css">
</head>
<div class="col-lg-10 col-lg-offset-1">
    <div>
        <legend><h2>Ajouter concert</h2></legend><br>
    </div>
    <form action='#' method='post' name="ajouterConcert">
        <div class="form-group col-lg-offset-3">
            <label for="nomConcert">Nom du concert :</label>
            <input type="text" class="form-control" id="nomConcert" name="nomConcert" required placeholder="Entrez le nom du concert"
                   pattern="[A-Za-z]{3,20}" title="Entrez minimum trois lettres du nom du concert. (aucun caractere speciaux ni chiffre est valide)" >
            <p id="demo"></p>
        </div>
        <div class="form-group col-lg-offset-3">
            <label for="nbPlaces">Nombres des places :</label>
            <input type="text" class="form-control" id="nbPlaces" name="nbPlaces" required placeholder="Entrez un nombre de places"
                   pattern="[0-9]{2,20}" title="Entrez minimum deux chiffres">
        </div>
        <div class="form-group col-lg-offset-3">
            <label for="nbArtistes">Nombre d'artistes :</label>
            <input type="text" class="form-control" id="nbArtistes" name="nbArtistes" required placeholder="Entrez un nombre de artistes"
                   pattern="[0-9]{1,20}" title="Entrez minimun un nombre">
        </div>
        <div class="form-group col-lg-offset-3">
            <label for="dateConcert">Date Concert :</label>
            <input type="date" max="2020-12-31" min="<?php echo date("Y-m-d",strtotime("+1 week"))?>"
                   class="form-control" id="dateConcert" name="dateConcert" required="true" placeholder="Entrez la date du concert">
        </div>
        <div class="form-group col-lg-offset-3">
            <label for="prixPlace">Prix place :</label>
            <input type="text" class="form-control" id="prixPlace" name="prixPlace" required placeholder="Entrez le prix du ticket."
                   pattern="[0-9.0-9]{1,20}" title="Entrez le prix du ticket.">
        </div>
        <div class="form-group col-lg-3 col-lg-offset-8">
            <button type="submit" name="button" class="buttonTable">Enregistrer</button><br>
        </div>
    </form>
</div>
<?php
include 'connexionDB.php';
$n =$p = $e=$u=$p ="";
if (isset($_POST['nomConcert'])) { $nC = $_POST['nomConcert']; }
if (isset($_POST['nbPlaces'])) { $nbP = $_POST['nbPlaces']; }
if (isset($_POST['nbArtistes'])) { $nbA = $_POST['nbArtistes']; }
if (isset($_POST['dateConcert'])) { $date = $_POST['dateConcert']; }
if (isset($_POST['prixPlace'])) { $prix = $_POST['prixPlace']; }
if(isset($_POST['button']) ){
$sql = "INSERT INTO concert (nomConcert, nbrePlaces, nbreArtistes, dateConcert, prixPlace)
VALUES ('$nC','$nbP','$nbA','$date','$prix')";

if (!$conn->query($sql)) {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
}
$conn->close();
?>