<?php session_start();

?>
<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <img src="resources/images/concert.jpg"  class="img-responsive" />
    </div>
</div>
<nav class="navbar navbar-inverse nav col-lg-10 col-lg-offset-1">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand">Concert's Ecode</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="?menu=index">Home</a></li>
      <li><a href="?menu=concerts">A l'affiche</a></li>
        <?php if (isset($_SESSION['userRoleAct']) && $_SESSION['userRoleAct'] == 'ROLE_ADMIN'){ ?>
    </ul>
      <ul class="nav navbar-nav">
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestion des concerts <span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li><a href="?menu=ajoutConcert">Ajouter concert</a></li>
                  <li><a href="?menu=listeConcert">Liste concert</a></li>
              </ul>
          </li>
      </ul>
      <ul class="nav navbar-nav">
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestion des clients <span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li><a href="?menu=listeUser">Liste client</a></li>
              </ul>
          </li>
      </ul>

      <ul  class="nav navbar-nav navbar-right">
          <li class="dropdown active"><a class="dropdown-toggle" data-toggle="dropdown" href="#" >Welcome <?php echo $_SESSION['userNameAct']; ?><span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li><a href="?menu=updateUser">Modifier</a></li>
              </ul>
          </li>
          <li><a href="?menu=logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
      <ul class="nav navbar-nav">
          <?php } else if (isset($_SESSION['userRoleAct']) && $_SESSION['userRoleAct'] == 'ROLE_USER'){ ?>
      </ul>
      <ul  class="nav navbar-nav navbar-right">
          <li class="dropdown active"><a class="dropdown-toggle" data-toggle="dropdown" href="#" >Welcome <?php echo $_SESSION['userNameAct']; ?><span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li><a href="?menu=updateUser">Modifier</a></li>
              </ul>
          </li>
          <li><a href="?menu=logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
      <ul class="nav navbar-nav">
          <?php }else if (isset($_SESSION)){ ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
          <?php include '/user/inscription.php';?>
          <?php include '/user/login.php';?>
      </ul >
      <?php } ?>
  </div>
</nav>