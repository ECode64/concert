<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "concert2";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = "CREATE TABLE IF NOT EXISTS user (
userId INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
nom VARCHAR(30) NOT NULL,
prenom VARCHAR(30) NOT NULL,
email VARCHAR(50) NOT NULL,
username VARCHAR(30) NOT NULL,
password VARCHAR(255) NOT NULL,
enabled INT (10) DEFAULT '1',
reg_date TIMESTAMP,
UNIQUE INDEX `username_unique` (`username`),
UNIQUE INDEX `email_unique` (`email`))";
if (!$conn->query($sql)) {
    echo "Error creating table user: " . $conn->error;
}
$sql = "CREATE TABLE IF NOT EXISTS concert (
concertId INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
nomConcert VARCHAR(30) NOT NULL,
nbrePlaces INT(6) UNSIGNED NOT NULL,
nbreArtistes INT(6) UNSIGNED NOT NULL,
enabled INT (10) DEFAULT '1',
dateconcert DATE NOT NULL,
prixPlace FLOAT NOT NULL,
UNIQUE INDEX `nomConcert_unique` (`nomConcert`)
)";

if (!$conn->query($sql)) {
    echo "Error creating table user_role: " . $conn->error;
}
$sql = "CREATE TABLE IF NOT EXISTS ticket(
ticketId INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
userId INT(6) UNSIGNED NOT NULL,
nomConcert VARCHAR(30) NOT NULL,
dateconcert DATE NOT NULL,
enabled INT (10) DEFAULT '1',
prix FLOAT NOT NULL)";
if (!$conn->query($sql)) {
    echo "Error creating table ticket : " . $conn->error;
}
$sql = "CREATE TABLE IF NOT EXISTS user_role (
user_role_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
role VARCHAR(30) NOT NULL,
userId INT(6) UNSIGNED NOT NULL);";

if (!$conn->query($sql)) {
    echo "Error creating table user_role: " . $conn->error;
}
?>